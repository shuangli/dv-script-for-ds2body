def fillTuple( tuple, myBranches, myTriggerList ):

    tuple.Branches = myBranches
        
    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolANNPID" ,
        "TupleToolPrimaries",
        "TupleToolPropertime",
        "TupleToolRecoStats",
        "TupleToolTrackInfo"
        ]

    # RecoStats for filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True

    from Configurables import TupleToolTISTOS, TupleToolDecay
    tuple.addTool(TupleToolDecay, name = 'Dp')
    
    # TISTOS for Dp
    tistos = tuple.addTupleTool('TupleToolTISTOS')
    tistos.TriggerList = myTriggerList
    tistos.FillHlt2 = False
    tistos.Verbose = True
#    tuple.Dp.ToolList+=[ "TupleToolTISTOS" ]
#    tuple.Dp.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
#    tuple.Dp.TupleToolTISTOS.Verbose=True
#    tuple.Dp.TupleToolTISTOS.TriggerList = myTriggerList

    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()",
        "phi"                  : "PHI"
        }
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)
    fit = tuple.Dp.addTupleTool('TupleToolDecayTreeFitter/ReFit')
    fit.constrainToOriginVertex = True
    fit.Verbose= True
    fit.UpdateDaughters = True
    
# DecayTreeTuple
from Configurables import DstConf, TurboConf # necessary for DaVinci v40r1 onwards
#TurboConf().PersistReco=True
#DstConf().Turbo=True

#from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *

myTriggerList = [
    # L0
    "L0ElectronDecision",
    "L0PhotonDecision",
    "L0HadronDecision",
    "L0MuonDecision",
    "L0MuonHighDecision",
    "L0DiMuonDecision",
    "L0PhysDecision",
    "L0GlobalDecision",
    
    # Hlt1 track    
    "Hlt1TrackAllL0Decision",
    "Hlt1TrackMuonDecision",
    "Hlt1TrackPhotonDecision",
    "Hlt1GlobalDecision",

    "Hlt1TrackMVADecision",
    "Hlt1TwoTrackMVADecision"
    ]


"""
"""
Dp2KmKpPip_Branch = {
    "Km"    :  "[ D_s+ -> ^K- K+ pi+ ]CC",
    "Kp"    :  "[ D_s+ -> K- ^K+ pi+ ]CC",
    "Pip1"    :  "[ D_s+ -> K- K+ ^pi+  ]CC",
    "Ds"    :  "^([ D_s+ -> K- K+ pi+ ]CC)"
}

the_line = "Hlt2CharmHadDspToKmKpPipTurbo/Particles"
Dp2KmKpPip = DecayTreeTuple("Ds2KmKpPip")
Dp2KmKpPip.WriteP2PVRelations = False
Dp2KmKpPip.RootInTES = "/Event/Charmspec/Turbo"
Dp2KmKpPip.Decay ="[ D_s+ -> ^K- ^K+ ^pi+ ]CC"
Dp2KmKpPip.Inputs = [ the_line ]
fillTuple( Dp2KmKpPip,
           Dp2KmKpPip_Branch,
           myTriggerList )


"""
Options for building Stripping
"""
from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

#from PhysConf.Selections import MomentumScaling, AutomaticData, TupleSelection
#my_selection = AutomaticData ( the_line )
#my_selection = MomentumScaling ( my_selection, Turbo = True, Year = '2017' ) 
#
## create Selection sequence
#from PhysConf.Selections import SelectionSequence
#selseq = SelectionSequence ('MomScale', my_selection).sequence()
#
from Configurables import CondDB
CondDB ( LatestGlobalTagByDataType = '2017')

from Configurables import DaVinci
#DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().InputType = 'MDST'                   # Addition for DaVinci v42r2 onwards 
DaVinci().RootInTES = '/Event/Charmspec/Turbo'           # Addition for DaVinci v42r2 onwards 
DaVinci().Turbo = True                         # Addition for DaVinci v42r2 onwards
DaVinci().EvtMax =  -1                        # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = "2017"
DaVinci().Simulation    = False
#DaVinci().DDDBtag   = "dddb-20150724"          # TEST
#DaVinci().CondDBtag = "cond-20170120-1"        # TEST
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [ ]
#DaVinci().UserAlgorithms += [ scaler ]
#DaVinci().UserAlgorithms += [ selseq ]
DaVinci().UserAlgorithms += [ Dp2KmKpPip ]   # The algorithms
#DaVinci().Input = [ "./00066595_00006085_1.charmspec.mdst" ]
# MDST


# Get Luminosity
DaVinci().Lumi = True
DaVinci().InputType = "MDST"

#from Configurables import LHCbApp
#LHCbApp().DDDBtag   = "dddb-20150724"
#LHCbApp().CondDBtag = "cond-20170120-1"
